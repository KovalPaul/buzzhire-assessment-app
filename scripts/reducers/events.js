import * as types from '../constants/ActionTypes';

const initialState = {
  events: [],
  isFetchingEvents: false
};

export default function events(state = initialState, action) {
  switch (action.type) {
    case types.ADD_EVENT: {
      return Object.assign({}, state, {
        events: [...state.events, action.payload]
      });
    }
    case types.RECIEVE_EVENTS: {
      return Object.assign({}, state, {
        events: action.payload
      });
    }
    case types.START_FETCHING_EVENTS: {
      return Object.assign({}, state, {
        isFetchingEvents: true
      });
    }
    case types.STOP_FETCHING_EVENTS: {
      return Object.assign({}, state, {
        isFetchingEvents: false
      });
    }
    default:
      return state;
  }
}
