import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import DateTime from 'react-datetime';
import 'react-datetime/css/react-datetime.css';
import EventCategoryPicker from './EventCategoryPicker';
import * as eventsAPI from '../api/eventsAPI';
import { addEvent } from '../actions/eventsActions';

const propTypes = {
  dispatch: PropTypes.func.isRequired
};

class EventAdder extends Component {

  constructor(props) {
    super(props);
    this.state = {
      label: '',
      start: null,
      end: null,
      category: null
    };

    this.handleLabelInputChange = this.handleLabelInputChange.bind(this);
    this.handleStartDateTimeChange = this.handleStartDateTimeChange.bind(this);
    this.handleEndDateTimeChange = this.handleEndDateTimeChange.bind(this);
    this.handleCategoryPickerChange = this.handleCategoryPickerChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleLabelInputChange(event) {
    this.setState({ label: event.target.value });
  }

  handleStartDateTimeChange(date) {
    this.setState({ start: date });
  }

  handleEndDateTimeChange(date) {
    this.setState({ end: date });
  }

  handleCategoryPickerChange(category) {
    this.setState({ category });
  }

  handleSubmit(e) {
    e.preventDefault();
    const { dispatch } = this.props;
    const event = {
      label: this.state.label,
      start: this.state.start,
      end: this.state.end,
      category: this.state.category
    };
    eventsAPI.addEvent(event)
      .then(newEvent => {
        dispatch(addEvent(newEvent));
        this.setState({
          label: '',
          start: null,
          end: null,
          category: null
        });
        return newEvent;
      })
      .catch(err => {
        // TODO handle error here
      });
  }

  validateEvent() {
    const { label, start, end, category } = this.state;
    const startIsValid = start && moment(start, 'YYYY-MM-DDTHH:mm:ss', true).isValid();
    const endIsValid = end && moment(end, 'YYYY-MM-DDTHH:mm:ss', true).isValid();
    return !!(label && startIsValid && endIsValid && category);
  }

  render() {
    const isValid = this.validateEvent();
    return (
      <div className="event-adder">
        <form onSubmit={this.handleSubmit}>
          <div className="event-adder__inner">
            <div className="event-adder__left-column">
              <label>
                <span>Label:</span>
                <input type="text" value={this.state.label} onChange={this.handleLabelInputChange} />
              </label>
              <label>
                <span>Select a category:</span>
                <EventCategoryPicker
                  value={this.state.category}
                  onChange={this.handleCategoryPickerChange}
                />
              </label>
            </div>
            <div className="event-adder__left-column">
              <label>
                <span>Start date/time:</span>
                <DateTime
                  value={this.state.start}
                  onChange={this.handleStartDateTimeChange}
                />
              </label>
              <label>
                <span>End date/time:</span>
                <DateTime
                  value={this.state.end}
                  onChange={this.handleEndDateTimeChange}
                />
              </label>
            </div>
            <div className="event-adder__button-wrap">
              <button type="submit" disabled={!isValid}>Add Event</button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

EventAdder.propTypes = propTypes;

function mapStateToProps(state) {
  const { events } = state.events;
  return { events };
}

export default connect(mapStateToProps)(EventAdder);
