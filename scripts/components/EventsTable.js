import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { fetchEvents } from '../actions/eventsActions';

const propTypes = {
  dispatch: PropTypes.func.isRequired,
  events: PropTypes.array.isRequired,
  isFetchingEvents: PropTypes.bool.isRequired
};

class EventsTable extends Component {

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(fetchEvents());
  }

  renderLoader() {
    return <div>Loading...</div>
  }

  renderEventsTable() {
    const { events } = this.props;
    const tableRows = events.map((event) =>
      <tr key={event.id}>
        <td>{event.label}</td>
        <td>{event.start.format('LLL')}</td>
        <td>{event.end.format('LLL')}</td>
        <td>
          <span
            className="events-table__category-indicator"
            style={{ background: event.category }}
          />
        </td>
      </tr>
    );
    return (
      <table className="events-table">
        <tbody>
          <tr>
            <th>Label:</th>
            <th>Start date:</th>
            <th>End date:</th>
            <th>Category:</th>
          </tr>
          {tableRows}
        </tbody>
      </table>
    );
  }

  render() {
    const { isFetchingEvents } = this.props;
    if (isFetchingEvents) {
      return this.renderLoader();
    }
    return this.renderEventsTable();
  }
}

EventsTable.propTypes = propTypes;

function mapStateToProps(state) {
  const { events, isFetchingEvents } = state.events;
  return { events, isFetchingEvents };
}

export default connect(mapStateToProps)(EventsTable);
