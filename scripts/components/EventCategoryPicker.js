import React, { Component, PropTypes } from 'react';
import { fetchEvents } from '../actions/eventsActions';
import categories from '../constants/EventCategories';

const propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func
};

class EventCategoryPicker extends Component {

  handleCategoryItemClick(value) {
    const { onChange } = this.props;
    if (onChange) {
      onChange(value);
    }
  }

  render() {
    const { value } = this.props;
    const categoryItems = categories.map(category => {
      const className = `category-picker__item ${value === category ? 'category-picker__item--active' : ''}`;
      return (
        <div
          key={category}
          className={className}
          onClick={() => this.handleCategoryItemClick(category)}
          style={{ background: category }}
        />
      );
    });
    return (
      <div className="category-picker">
        <div className="category-picker__inner">
          { categoryItems }
        </div>
      </div>
    );
  }
}

EventCategoryPicker.propTypes = propTypes;

export default EventCategoryPicker;
