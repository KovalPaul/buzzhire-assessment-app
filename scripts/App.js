import React, { Component } from 'react';
import { BrowserRouter, Route, Redirect } from 'react-router-dom';
import EventsPage from './pages/EventsPage';

class App extends Component {

  render() {
    return (
      <BrowserRouter>
        <div>
          <Route exact path="/" render={() => <Redirect to="/events" />} />
          <Route path="/events" component={EventsPage} />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;

