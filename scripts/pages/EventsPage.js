import React, { Component, PropTypes } from 'react';
import { Link, Route } from 'react-router-dom';
import EventsTable from '../components/EventsTable';
import EventAdder from '../components/EventAdder';

const propTypes = {
  match: PropTypes.object.isRequired
};

class EventsPage extends Component {

  renderAddEventSection() {
    const { match } = this.props;
    return (
      <div>
        <Link className="add-event-visibility-trigger" to={`${match.url}`}>Hide</Link>
        <EventAdder />
      </div>
    );
  }

  renderShowAddEventSectionButton() {
    const { match } = this.props;
    return (
      <Link className="add-event-visibility-trigger" to={`${match.url}/add`}>Add Event</Link>
    );
  }

  render() {
    const { match } = this.props;
    return (
      <div className="events-page-layout">
        <div className="events-page-layout__inner">
          <div className="events-page-layout__left-column">
            <EventsTable />
          </div>
          <div className="events-page-layout__right-column">
            <div className="events-page-layout__sticky">
              <Route
                exact
                path={`${match.url}`}
                render={this.renderShowAddEventSectionButton.bind(this)}
              />
              <Route
                exact
                path={`${match.url}/add`}
                render={this.renderAddEventSection.bind(this)}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

EventsPage.propTypes = propTypes;

export default EventsPage;
