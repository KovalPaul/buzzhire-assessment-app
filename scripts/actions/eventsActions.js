import * as types from '../constants/ActionTypes';
import * as eventsAPI from '../api/eventsAPI';

export function fetchEvents() {
  return dispatch => {
    dispatch(startFetchingEvents());
    eventsAPI.fetchEvents()
      .then(events => {
        dispatch(receiveEvents(events));
        dispatch(stopFetchingEvents());
        return events;
      })
      .catch(err => {
        // TODO handle error here
      });
  };
}

export function receiveEvents(events) {
  return {
    type: types.RECIEVE_EVENTS,
    payload: events,
  };
}

export function startFetchingEvents() {
  return {
    type: types.START_FETCHING_EVENTS
  };
}

export function stopFetchingEvents() {
  return {
    type: types.STOP_FETCHING_EVENTS
  };
}

export function addEvent(event) {
  return {
    type: types.ADD_EVENT,
    payload: event,
  };
}

