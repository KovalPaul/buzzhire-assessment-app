import axios from 'axios';
import moment from 'moment';

export function fetchEvents() {
  return axios.get('https://assessments.bzzhr.net/calendar/')
    .then(response => {
      const originalEvents = response.data;
      const convertedEvents = originalEvents.map(event => {
        return Object.assign({}, event, {
          start: moment(event.start),
          end: moment(event.end)
        });
      });
      return convertedEvents;
    });
}

export function addEvent(event) {
  // TODO implement real call for adding event
  return Promise.resolve(Object.assign({}, event, { id: Math.floor(Math.random()) }));
}
